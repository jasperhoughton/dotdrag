/*
	dotDrag.JS
*/

//-----------------------------------------------------------------------------
// Variables
//-----------------------------------------------------------------------------

var selected = null;
var x = 0, y = 0;

var baseZ = 1;
var topZ = 2;

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

/// <summary>
/// Initialises all the draggable elements.
/// </summary>
function init ()
{
	var elements = document.getElementsByClassName ( 'draggable' );

	for ( var i = 0; i < elements.length; i++ )
	{
		elements[i].onmousedown = function ( e )
		{
			selected = this;
			x = selected.offsetLeft - e.pageX;
			y = selected.offsetTop - e.pageY;

			for ( var i = 0; i < elements.length; i++ )
			{
				elements[i].style.zIndex = baseZ;
			}

			selected.style.zIndex = topZ;
			selected.style.cursor = "move";

			return false;
		};
	}
}

/// <summary>
/// Handles the dragging.
/// </summary>
function drag ( e )
{
	if (selected !== null)
	{
		selected.style.position = "absolute";
		selected.style.bottom = "auto";
		selected.style.right = "auto";
		selected.style.left = (x + e.pageX) + 'px';
		selected.style.top = (y + e.pageY) + 'px';
	}
}

/// <summary>
/// Nulls the selected element.
/// </summary>
function destroy ()
{
	if ( selected != null )
	{
		selected.style.cursor = "default";
		selected = null;
	}
}

//-----------------------------------------------------------------------------
// Events
//-----------------------------------------------------------------------------

document.onload = init ();
document.onmousemove = drag;
document.onmouseup = destroy;
