# dotDrag #

---

A super simple, super small JavaScript library that enables draggable elements.

## Setup ##

Add the library to your page and call `elementname.draggable()` to make the element draggable. Calling `elementname.draggable.destroy()` will release all events and revert the element back to its original, undraggable state.

### Functions ###

The following functions are exposed:

- `draggable()` – Initializes dotDrag on the specified element.
- `draggable.destroy()` – Destroys the specified instance of dotDrag, releasing all events.